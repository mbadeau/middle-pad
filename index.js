'use strict';
module.exports = middlePad;

function middlePad(str, maxLength, ch) {
  str = str + '';
  ch = ch + '';
  let pad = ch.repeat(str.length - maxLength);
  try (parseInt(maxLength)){
      let randomIdx = Math.random() * maxLength;
      str = str.slice[0,randomIdx - 1] + pad + str.slice[randomIdx, str.length];
    }
  } catch (err) {
    throw err;
  }
  return str;
}

